-- print("Hello world")

-- require("http_client")
-- http_client.request("GET", "https://api.sampleapis.com/avatar/info", {})
--
vim.cmd("source ~/.config/nvim/lua/http_client/init.lua")
require("http_client")
vim.cmd("source ~/.config/nvim/lua/string_utils/init.lua")
-- http_client.request("GET", "https://api.sampleapis.com/avatar/info", {})
print(vim.inspect(http_client.request("GET", "https://api.sampleapis.com/avatar/info", {})))

-- print("--- test 1 ---")
-- print(string.substr("abcdef", 1))
-- substr('abcdef', 1);     // bcdef
-- print("--- test 2 ---")
-- print(string.substr("abcdef", 1, 3))
-- substr('abcdef', 1, 3);  // bcd
-- print("--- test 3 ---")
-- substr('abcdef', 0, 4);  // abcd
-- print(string.substr("abcdef", 0, 4))
-- print("--- test 4 ---")
-- substr('abcdef', 0, 8);  // abcdef
-- print(string.substr("abcdef", 0, 8))
-- print("--- test 5 ---")
-- print(string.substr("abcdef", -1, 1))
-- substr('abcdef', -1, 1); // f
-- print(string.substr("> bla bla bla bla", 2))

