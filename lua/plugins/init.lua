return {
  {
    "stevearc/conform.nvim",
    event = 'BufWritePre', -- uncomment for format on save
    config = function()
      require "configs.conform"
    end,
  },

  -- These are some examples, uncomment them if you want to see them work!
  {
    "neovim/nvim-lspconfig",
    config = function()
      require("nvchad.configs.lspconfig").defaults()
      require "configs.lspconfig"
    end,
  },

  {
    "williamboman/mason.nvim",
    opts = {
      ensure_installed = {
        "lua-language-server",
        "stylua",
        "html-lsp",
        "css-lsp",
        "prettier",
      },
    },
  },

  {
    "nvim-treesitter/nvim-treesitter",
    opts = {
      ensure_installed = {
        "vim",
        "lua",
        "vimdoc",
        "html",
        "css",
        "php",
        "dockerfile",
        "javascript",
        "bash",
        "comment",
        "csv",
        "diff",
        "fish",
        "git_config",
        "git_rebase",
        "gitattributes",
        "gitcommit",
        "gitignore",
        "go",
        "ini",
        "jsdoc",
        "just",
        "luadoc",
        "markdown",
        "markdown_inline",
        "phpdoc",
        "regex",
        "scss",
        "sql",
        "ssh_config",
        "tmux",
        "toml",
        "twig",
        "typescript",
        "v",
        "vim",
        "xml",
        "yaml",
      },
    },
  },
  {
    "folke/trouble.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    opts = {
      position = "right",
      width = 80,
      mode = 'document_diagnostics'
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
    },
  },
  {
    'LhKipp/nvim-nu'
  },
  { "folke/neodev.nvim", opts = {} }

  -- {
  -- "folke/noice.nvim",
  -- event = "VeryLazy",
  -- opts = {
  --   -- add any options here
  -- },
  -- dependencies = {
  --   -- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
  --   "MunifTanjim/nui.nvim",
  --   -- OPTIONAL:
  --   --   `nvim-notify` is only needed, if you want to use the notification view.
  --   --   If not available, we use `mini` as the fallback
  --   "rcarriga/nvim-notify",
  --   }
  -- }
}
