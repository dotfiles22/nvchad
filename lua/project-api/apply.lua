

-- @param path string|nil
-- @return table|nil
return function (config)
  local fn = vim.fn

  local path = fn.stdpath('data') .. '/project.json'

  local is_config = fn.filereadable(path) == 1

  if is_config ~= true then
    vim.notify_once("[ERROR] No file in " .. path)
  else
    local file = io.open(path, 'w+')

    if file == nil then
      return nil
    end

    local json = vim.json.encode(config)

    file.write(file, json)
    file.close(file)
  end

  return is_config
end

