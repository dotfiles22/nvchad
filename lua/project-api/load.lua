

---@param path string|nil
---@return table|nil
return function (path)
  local fn = vim.fn

  -- Default path
  if path == nil then
    path = fn.stdpath('data') .. '/project.json'
  end

  local is_config = fn.filereadable(path) == 1
  -- print(vim.inspect(is_config))
  if is_config ~= true then
    vim.notify_once("[INFO] No project configuration in " .. path)
    local file = io.open(path, 'w+')

    if file == nil then
      -- print('null')
      return nil
    end

    file.write(file, "{}")
    file.close(file)

    return {}

  else
    local file = io.open(path, 'r+')

    if file == nil then
      return nil
    end

    local fileContent = file.read(file, "a")
    local config = vim.json.decode(fileContent)
    return config
  end
end

