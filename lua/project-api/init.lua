local M = {}

local cmd = vim.cmd
local fs = vim.fs
local g = vim.g
local opt = vim.opt

---Define a new project
---@param title string
---@param config table
M.add = function(title, config)
  local data = M.load()

  local data_len = 0
  for _ in pairs(data) do
    data_len = data_len + 1
  end

  if type(data) == "table" then
    data[data_len] = title
  end

  M.apply(data)

  return M
end

M.apply = require "project-api.apply"

M.setup = function(config)
  -- print(vim.inspect(config))
end

M.load = require "project-api.load"

function M.set_pwd(dir, method)
  if dir ~= nil then
    if vim.fn.getcwd() ~= dir then
      local scope_chdir = config.options.scope_chdir
      if scope_chdir == "global" then
        vim.api.nvim_set_current_dir(dir)
      elseif scope_chdir == "tab" then
        vim.cmd("tcd " .. dir)
      elseif scope_chdir == "win" then
        vim.cmd("lcd " .. dir)
      else
        return
      end
    end
    return true
  end

  return false
end

_G.project = M

return M
