vim.api.nvim_create_autocmd("BufWritePost", {
  pattern = "test/*.lua",
  callback = function(event)
    local absolute_filepath = event.match
    local filename = event.file
    local buffer_id = event.buf

    local oldPrint = print

    local new_buffer_id = vim.api.nvim_create_buf(true, true)
    print = function(content)
      local buf_lines = vim.api.nvim_buf_get_lines(new_buffer_id, 0, -1, true)
      -- if (type(buf_lines) ~= "table") then
        -- buf_lines = {}
      -- end

      local buf_current_index = (#buf_lines) - 1
      -- print(buf_current_index)

      -- if buf_current_index <= 0 then
        -- buf_current_index = 0
      -- end

      content = string.explode("\n", content)

      -- if type(content) ~= "table" then
        -- oldPrint("ERROR content is type " .. type(content))
      -- else
        -- for i, value in pairs(content) do
          -- if type(value) ~= "string" then
            -- oldPrint("ERROR value is type " .. type(value))
            -- table.remove(content, i)
          -- end
        -- end
        -- oldPrint("-----")
        -- oldPrint(type(new_buffer_id) .. " " .. tostring(new_buffer_id))
        -- oldPrint(type(buf_current_index) .. " " .. tostring(buf_current_index))
        -- oldPrint(type(content) .. " " .. vim.inspect(content))
        -- oldPrint("-----")

        vim.api.nvim_buf_set_lines(
          new_buffer_id,
          buf_current_index,
          buf_current_index,
          false,
          content
        )
      -- end
      -- oldPrint("[Hello]" .. content)
    end

    vim.api.nvim_buf_set_option(new_buffer_id, 'filetype', "RunnerOutput")

    -- local win_info = vim.fn.getwininfo()

    vim.cmd('vsplit')
    vim.cmd(string.format('buffer %d', new_buffer_id))
    vim.cmd(string.format('source %s', absolute_filepath))
    vim.cmd(string.format('file %s', "Output"))

    -- print(vim.inspect(win_info))
    -- print(vim.inspect(event))

    print = oldPrint
  end
})
