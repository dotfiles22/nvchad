--- @enum (key) METHOD Method used for the call
local METHOD = {
  DELETE = "DELETE",
  GET = 'GET',
  HEAD = 'HEAD',
  OPTIONS = 'OPTIONS',
  PATCH = 'PATCH',
  POST = 'POST',
  PUT = 'PUT'
}

--- @class HttpError
--- @field type HttpErrorType


--- @class HttpRequestOptions
--- @field Authentification string?

--- @enum (key) HttpErrorType
local HttpErrorType = {
  "request",
  "response"
}


