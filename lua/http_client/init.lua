--- @module 'http_client.meta'

local curl_wrapper = require('http_client.wrapper.curl')

--- @class HttpResponse
--- @field code integer Response code
--- @field headers table Headers
--- @field content table Body sent back

local M = {}

--- @param method METHOD
--- @param url string
--- @param opt HttpRequestOptions
--- @return HttpResponse|HttpError
--- - @nodiscard
M.request = function(method, url, opt)

  opt['method'] = method
  local response = curl_wrapper.query(url, opt)
  return response
  --- @type HttpResponse
  -- local httpresponse = {
    -- code = 200,
    -- headers = {
    -- },
    -- content = {
    -- },
  -- }

  -- return httpresponse;
end

M.global_options = {
  secure = true, -- Use and check certificates (For https only)
  timeout = 20 -- Timeout in seconds
}

--- @class HttpClient
--- @field request function
_G.http_client = M
