local M = {}

require('string_utils')

M.query = function(url, opt)
  local command = "curl --verbose --no-styled-output --no-progress-meter " .. url .. " 2>&1"
  print(command)
  local handle = io.popen(command, "r")

  if handle == nil then
    return nil
  end

  local result = M.parse_output(handle)
  -- local result = handle:read("*a")

  -- handle:close()

  -- result = M.parse_output(result)

  return result
  -- return vim.fn.system()
end


--- Parse curl command output
--- @param handle file*
M.parse_output = function(handle)
  local request_headers = {}
  local response_headers = {}
  local unknown = {}
  local content = {}
  local http_code = 0
  local protocol = ""
  local lines = handle:read("*a")
  local wlines = string.explode("\r\n", lines)

  for _, wline in ipairs(wlines) do
    lines = string.explode("\n", wline)

    for _, line in ipairs(lines) do
      if (string.start_with(line, "> ")) then
        line = string.substr(line, 2)
        -- Cleaning empty input
        if (line ~= "") then
          table.insert(request_headers, line)
          -- print("request header: " .. vim.inspect(line))
        end
      elseif (string.start_with(line, "< ")) then
        line = string.substr(line, 2)
        -- Cleaning empty input
        if (line ~= "") then
          local separator_pos = string.find(line, ": ")
          -- Response header
          if (separator_pos) then
            local value = string.substr(line, separator_pos)
            local title = string.substr(line, 0, separator_pos - 1)
            -- response_headers[title] = value
            table.insert(response_headers, value)
          else
            protocol = line
            http_code = string.explode(" ", protocol)[1]
          end
          -- print("response header: " .. vim.inspect(line))
        end
      elseif (string.start_with(line, "* ")) then
        line = string.substr(line, 2)
        -- Cleaning empty input
        if (line ~= "") then
          table.insert(unknown, line)
          -- print("other data: " .. vim.inspect(line))
        end
        -- Do nothing
      else
        table.insert(content, line)
      end
    end
  end

  return {
    header_data_sent = request_headers,
    header_data_received = response_headers,
    additional_info = unknown,
    content = content
  }
end

return M
