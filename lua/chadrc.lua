-- This file  needs to have same structure as nvconfig.lua 
-- https://github.com/NvChad/NvChad/blob/v2.5/lua/nvconfig.lua

local M = {}

M.ui = {
	theme = "catppuccin",
  nvdash = {
    load_on_startup = true
  },
  tabufline = {
    --  more opts
    -- order = { "treeOffset", "buffers", "tabs", "btns", 'abc' },
    order = { "treeOffset", "buffers", "tabs", "project" },
    lazyload = false,
    modules = {
      -- You can add your custom component
      project = function()
        return "project"
      end,
    }
  },
  statusline = {
    order = { "mode", "file", "diagnostics", "git", "%=", "lsp_msg", "%=", "lsp", "cursor", "cwd" }
  }
	-- hl_override = {
	-- 	Comment = { italic = true },
	-- 	["@comment"] = { italic = true },
	-- },
}

return M
