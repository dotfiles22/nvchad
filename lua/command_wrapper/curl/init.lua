local M = {

}

M.query = function (method, url, opt)
  local command = "curl" .. url
  local handle = io.popen(command, "r")

  if handle == nil then
    return nil
  end

  local result = handle:read("*a")

  handle:close()

  return result
end

M.handle_error = function ()
  
end

return M
