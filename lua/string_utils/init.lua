--- Checks if a string starts with a given substring
--- @param haystack string The string to search in
--- @param needle string The substring to search for in the haystack
--- @return boolean
--- @nodiscard
function string.start_with(haystack, needle)
  return string.sub(haystack, 1, string.len(needle)) == needle
end

--- Return part of a string
---
--- string.substr("abcdef", 1) => Returns "bcdef"
--- string.substr("abcdef", -1) => Returns "f"
--- string.substr("abcdef", -2) => Returns "ef"
--- string.substr("abcdef", -3, 1) => Returns "d"
--- @param input string The input string
--- @param offset integer If offset is non-negative, the returned string will start at the offset'th position in string, counting from zero. For instance, in the string 'abcdef', the character at position 0 is 'a', the character at position 2 is 'c', and so forth.
--- @param length ?integer
--- @return string substr The extracted part of string, or an empty string.
--- @nodiscard
function string.substr(input, offset, length)
  if offset < 0 then
    offset = string.len(input) + offset
  end

  if length == nil then
    length = string.len(input)
  else
    length = (offset + length)
  end

  offset = offset + 1

  return string.sub(input, offset, length)
end

--- Split a string by a string
--- @param separator string The boundary string
--- @param input string The input string
--- @nodiscard
function string.explode(separator, input)
  local result               = {}
  local from                 = 1
  local delim_from, delim_to = string.find(input, separator, from)
  while delim_from do
    table.insert(result, string.sub(input, from, delim_from - 1))
    from                 = delim_to + 1
    delim_from, delim_to = string.find(input, separator, from)
  end
  table.insert(result, string.sub(input, from))
  return result
end
