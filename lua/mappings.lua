require "nvchad.mappings"

-- TODO : Implement
-- local keymaps = vim.api.nvim_get_keymap("n")

-- Deleting every plugins / distribution keymap
--
-- for _, keymap in pairs(keymaps) do
-- local is_leader = string.gmatch
-- if string.match(keymap.lhs, "leader") ~= nil then
-- print(keymap.lhs)
-- end
-- print(keymap.lhs)
-- end

local unmap = vim.keymap.del

-- Toggle relative linue number
unmap({ "n" }, "<leader>rn")
-- Toggle NvChad themes
unmap({ "n" }, "<leader>th")
unmap("n", "<leader>x")
unmap("n", "<leader>b")
--unmap("n", "<leader>D")
unmap("n", "<leader>n")
unmap("n", "<leader>q")
unmap("n", "<leader>ch")
unmap("n", "<leader>cm")
unmap("n", "<C-n>")
unmap("n", "<leader>fm")

local map = vim.keymap.set

map("n", "<leader>d", function()
  require("nvchad.tabufline").close_buffer()
end, { desc = "Close buffer" })
map("n", "<C-c>", function()
  require("Comment.api").toggle.linewise.current()
end, { desc = "Comment Toggle" })
map(
  "v",
  "<C-c>",
  "<ESC><cmd>lua require('Comment.api').toggle.linewise(vim.fn.visualmode())<CR>",
  { desc = "Comment Toggle" }
)

map("n", "<leader>cf", function()
  require("conform").format { lsp_fallback = true }
end, { desc = "Format Files" })

map("n", "U", "<C-r>", { desc = "CMD enter command mode" })
map("n", ";", ":", { desc = "CMD enter cmmand mode" })
map("i", "jk", "<ESC>")

-- Delete
map({ "n", "v" }, "d", "x", { desc = "Delete char or selection" })
-- Select line
map("n", "x", "V", { desc = "Select line" })
map("v", "x", "j", { desc = "Select next line" })

-- Normal selection
map("n", "e", "ve", { desc = "Normal select to end of word" })
map("v", "e", "<ESC>ve", { desc = "Normal select to end of word" })

-- word after
map("n", "w", "vw", { desc = "Normal select until next word" })
map("v", "w", "<ESC>vw", { desc = "Normal select until next word" })
-- word before map("n", "b", "vb", { desc = "Normal select previous word"})
map("n", "b", "vb", { desc = "Normal select previous word" })
map("v", "b", "<ESC>vb", { desc = "Normal select previous word" })
map({ "n", "t" }, "<leader>tt", function()
  local width_ratio = 0.7
  local height_ratio = 0.8
  local win_width = vim.opt.columns:get()
  local win_height = vim.opt.lines:get()
  local modal_width = math.floor(win_width * width_ratio)
  local modal_height = math.floor(win_height * height_ratio)
  local offset_width = math.floor((win_width - modal_width) / 8)
  print(offset_width)
  require("nvchad.term").toggle({ pos = "float", id = "floatTerm", float_opts = {width =  width_ratio, height =  height_ratio, row =  0, col = offset_width }  })
end, { desc = "Toggle Terminal Floating term" })

map("n", "<leader>td", function ()
  require("trouble").toggle()
end, {desc = "Toogle diagnostics"})
map("n", "<leader>tD", function() require("trouble").toggle("workspace_diagnostics") end)
-- map("n", "miw", "veb")

-- Match

--
-- Terminal
--

--
-- Window
--
map({ "n", "t" }, "<C-h>", "<C-w>h")
map({ "n", "t" }, "<C-j>", "<C-w>j")
map({ "n", "t" }, "<C-k>", "<C-w>k")
map({ "n", "t" }, "<C-l>", "<C-w>l")

map({ "n" }, "H", ":bprev<CR>")
map({ "n" }, "L", ":bnext<CR>")
--
-- Goto
--
map({ "n", "v" }, "gl", "$", { desc = "Goto right line" })
map({ "n", "v" }, "gh", "0", { desc = "Goto left line" })
map({ "n", "v" }, "gs", "^", { desc = "Goto start line" })
map({ "n", "v" }, "ge", "G$", { desc = "Goto end file" })

map("n", "%", "ggvG$")
--
-- Find
--
map("n", "<leader>ff", ":Telescope git_files<CR>", { desc = "Find files" })
map("n", "<leader>fc", ":Telescope commands<CR>", { desc = "Find commands" })
